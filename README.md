# WebHotelHub - Sistema de Gestión Hotelera

WebHotelHub es un sistema de gestión hotelera diseñado para facilitar la administración de hoteles. Desarrollado en Java 11 con Spring Boot y gestionado con Maven, este sistema proporciona una solución integral para las necesidades diarias de gestión hotelera.

## Descripción

El sistema WebHotelHub permite a los administradores de hoteles gestionar reservas, habitaciones, clientes, empleados y otros aspectos esenciales del negocio hotelero. Con una interfaz amigable y fácil de usar, se convierte en una herramienta indispensable para cualquier establecimiento hotelero que busque optimizar sus operaciones y ofrecer un servicio de calidad a sus clientes.

## Características Principales

- **Gestión de Reservas:** Permite realizar, modificar y cancelar reservas con facilidad.
- **Administración de Habitaciones:** Gestiona diferentes tipos de habitaciones, su disponibilidad y tarifas.
- **Base de Datos de Clientes:** Mantiene un registro de los clientes, sus preferencias y su historial de reservas.
- **Gestión de Empleados:** Administra el personal del hotel, sus roles y horarios.
- **Reportes:** Genera reportes detallados sobre ocupación, ingresos y otros indicadores clave.
- **Interfaz Amigable:** Diseñada para ser intuitiva y fácil de usar, reduciendo la curva de aprendizaje.

## Requisitos

- Java 11
- Maven
- PostgreSQL 15.x


## Cómo Ejecutar

1. Clonar el repositorio:
   ```bash
   git clone https://gitlab.com/uagrm1/mod5/clase05.git


## Configurar Conexión a la Base de Datos

Para poder ejecutar el proyecto de la rama *tdd-database-h2*, debe considerar la configuración de la base de datos que se encuentra en: *src/main/resources/application.properties

```bash
    # URL de conexión a la base de datos
    spring.datasource.url=jdbc:postgresql://localhost:5432/reservas_db
    spring.datasource.username=postgres
    spring.datasource.password=postgres
    spring.datasource.driver-class-name=org.postgresql.Driver
