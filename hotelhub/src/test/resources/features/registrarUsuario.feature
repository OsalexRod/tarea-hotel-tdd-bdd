# language: es
Característica: Registrar a un nuevo usuario
  Como un empleado del hotel y registrado en el sistema
  Quiero registrar a un nuevo usuario en el sistema del hotel
  Para que el nuevo usuario pueda tener acceso al hotel

  Antecedentes:
    Dado que soy un empleado registrado en el sistema
    Y me he logueo en el sistema con mis credenciales
    Y me dirijo hasta el formulario de registro de nuevo usuario

  Escenario: Registro exitoso de un nuevo usuario
    Cuando introduzco el nombre "Alejandro", apellido "Rodriguez" y ci "9794155" del nuevo usuario
    Y presiono el boton registrar nuevo usuario
    Entonces deberia ver un mensaje confirmando el registro "Registro realizado exitosamente!!!"