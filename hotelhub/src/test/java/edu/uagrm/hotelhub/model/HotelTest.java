package edu.uagrm.hotelhub.model;

import edu.uagrm.hotelhub.repository.HotelRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class HotelTest {

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    @Rollback(value = false)
    @Order(1)
    public void testGuardarHotel() {
        Hotel hotel = new Hotel("La Paz", "Av. 16 de Julio", 150.0, 4.5);
        Hotel hotelGuardado = hotelRepository.save(hotel);

        assertNotNull(hotelGuardado);
    }

    @Test
    @Order(2)
    public void testBuscarHotelPorNombre() {
        String nombre = "La Paz";

        Hotel hotel = hotelRepository.findByNombre(nombre);

        assertThat(hotel.getNombre()).isEqualTo(nombre);
    }

    @Test
    @Order(3)
    public void testBuscarHotelPorNombreNoExistente() {
        String nombre = "Royal";

        Hotel hotel = hotelRepository.findByNombre(nombre);

        assertNull(hotel);
    }

    @Test
    @Rollback(value = false)
    @Order(4)
    public void testActualizarHotel() {
        String nombre = "Copa Cabana";
        Hotel hotel = new Hotel(nombre, "Av. Camacho", 100.0, 4.0);
        hotel.setId(1); // ID del hotel a modificar

        hotelRepository.save(hotel);

        Hotel hotelActualizado = hotelRepository.findByNombre(nombre);
        assertThat(hotelActualizado.getNombre()).isEqualTo(nombre);
    }

    @Test
    @Order(5)
    public void testListarHoteles(){
        List<Hotel> hoteles  = (List<Hotel>) hotelRepository.findAll();

        for(Hotel hotel : hoteles){
            System.out.println(hotel);
        }

        assertThat(hoteles.size()).isGreaterThan(0);
    }

    @Test
    @Rollback(value = false)
    @Order(6)
    public void testEliminarHotel() {
        Integer id = 2;
        boolean esExistenteAntesDeEliminar = hotelRepository.findById(id).isPresent();
        hotelRepository.deleteById(id);

        boolean noExisteDespuesDeEliminar = hotelRepository.findById(id).isPresent();

        assertTrue(esExistenteAntesDeEliminar);
        assertFalse(noExisteDespuesDeEliminar);
    }
}