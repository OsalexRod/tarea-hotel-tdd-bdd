package edu.uagrm.hotelhub.stepDefinitions;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class RegistrarUsuarioStep {

    private final UsuarioModelo usuarioModelo = new UsuarioModelo();
    private boolean resultadoDeGuardarUsuario;

    @Dado("que soy un empleado registrado en el sistema")
    public void que_soy_un_empleado_registrado_en_el_sistema() {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
    }

    @Dado("me he logueo en el sistema con mis credenciales")
    public void me_he_logueo_en_el_sistema_con_mis_credenciales() {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
    }

    @Dado("me dirijo hasta el formulario de registro de nuevo usuario")
    public void me_dirijo_hasta_el_formulario_de_registro_de_nuevo_usuario() {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
    }

    @Cuando("introduzco el nombre {string}, apellido {string} y ci {string} del nuevo usuario")
    public void introduzco_el_nombre_apellido_y_ci_del_nuevo_usuario(String nombre, String apellido, String ci) {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
        this.resultadoDeGuardarUsuario = usuarioModelo.guardarUsuario(nombre, apellido, ci);
        assertTrue(this.resultadoDeGuardarUsuario);
    }

    @Cuando("presiono el boton registrar nuevo usuario")
    public void presiono_el_boton_registrar_nuevo_usuario() {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
    }

    @Entonces("deberia ver un mensaje confirmando el registro {string}")
    public void deberia_ver_un_mensaje_confirmando_el_registro(String mensajePositivo) {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
        if (this.resultadoDeGuardarUsuario) {
            System.out.println(mensajePositivo);
        } else {
            System.err.println("No se pudo hacer el registro del nuevo usuario...");
            assertTrue(this.resultadoDeGuardarUsuario);
        }
    }
}
