package edu.uagrm.hotelhub.stepDefinitions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Savepoint;

public class UsuarioModelo {
    private Conexion conexion;

    public UsuarioModelo() {
        this.conexion = new Conexion();
    }

    public boolean guardarUsuario(String nombre, String apellido, String ci) {
        Connection connection = this.conexion.getConnection();
        String query = "INSERT INTO public.usuarios (nombre, apellido, ci) VALUES (?, ?, ?);";
        try {
            Savepoint savepoint = connection.setSavepoint();
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, nombre);
                preparedStatement.setString(2, apellido);
                preparedStatement.setString(3, ci);
                if (preparedStatement.executeUpdate() == 0) {
                    throw new Exception("La declaracion no deviolvio ningun resultado");
                }
                connection.commit();
                System.out.println("usuario guardado exitosamente");
                return true;
            } catch (Exception exception) {
                connection.rollback(savepoint);
                System.err.println("ocurrio un problema al guardar el usuario");
                System.err.println(exception.getMessage());
                return false;
            }
        } catch (Exception exception) {
            System.err.println("ocurrio un problema al hacer BEGINTRANSACTION o al hacer ROLLBACK");
            System.err.println(exception.getMessage());
            return false;
        }
    }
}
