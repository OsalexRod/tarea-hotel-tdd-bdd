package edu.uagrm.hotelhub.repository;

import edu.uagrm.hotelhub.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
    public Usuario findByNombre(String nombre);
}
