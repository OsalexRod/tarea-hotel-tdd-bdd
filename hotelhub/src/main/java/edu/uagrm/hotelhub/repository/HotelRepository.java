package edu.uagrm.hotelhub.repository;

import edu.uagrm.hotelhub.model.Hotel;
import org.springframework.data.repository.CrudRepository;

public interface HotelRepository extends CrudRepository<Hotel, Integer> {

    public Hotel findByNombre(String nombre);
}
