package edu.uagrm.hotelhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebHotelHubApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebHotelHubApplication.class, args);
	}

}
