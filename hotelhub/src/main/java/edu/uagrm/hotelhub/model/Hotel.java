package edu.uagrm.hotelhub.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "hoteles")
public class Hotel {
    //Atibutos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    private String direccion;

    @Column(name = "precio_por_noche")
    private double precioPorNoche;

    private double calificacion;

    public Hotel() {

    }

    public Hotel(String nombre, String direccion, double precioPorNoche, double calificacion) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.precioPorNoche = precioPorNoche;
        this.calificacion = calificacion;
    }

    public Hotel(String nombre, String direccion, double precioPorNoche) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.precioPorNoche = precioPorNoche;
    }
}