CREATE TABLE public.hoteles
(
  id bigserial NOT NULL,
  nombre character varying(100) NOT NULL,
  direccion character varying(100) NOT NULL,
  precio_por_noche float NOT NULL,
  calificacion float NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE public.usuarios
(
  id bigserial NOT NULL,
  nombre character varying(20) NOT NULL,
  apellido character varying(30) NOT NULL,
  ci character varying(15) NOT NULL,
  PRIMARY KEY (id)
);